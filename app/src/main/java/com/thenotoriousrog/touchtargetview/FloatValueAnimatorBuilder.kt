package com.thenotoriousrog.touchtargetview

import android.animation.ValueAnimator
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.TimeInterpolator;
import android.R.animator



/**
 * Wrapper to provide a builer type interface.
 */
class FloatValueAnimatorBuilder {

    interface AnimatorUpdateListener {
        fun onUpdate(lerpTime: Float) // reports an update on the animation
    }

    interface AnimatorEndListener {
        fun onEnd() // reports finishing of the animation.
    }

    private lateinit var animator: ValueAnimator
    private var endListener: AnimatorEndListener? = null


    fun reverse(reverse: Boolean): FloatValueAnimatorBuilder {
        if(reverse) {
            this.animator = ValueAnimator.ofFloat(1.0f, 0.0f)
        } else{
            this.animator = ValueAnimator.ofFloat(0.0f, 1.0f)
        }
        return this
    }

    fun delayBy(millis: Long): FloatValueAnimatorBuilder {
        animator.startDelay = millis
        return this
    }

    fun duration(millis: Long): FloatValueAnimatorBuilder {
        animator.duration = millis
        return this
    }

    fun interpolator(lerper: TimeInterpolator): FloatValueAnimatorBuilder {
        animator.interpolator = lerper
        return this
    }

    fun repeat(times: Int): FloatValueAnimatorBuilder {
        animator.repeatCount = times
        return this
    }

    fun onUpdate(listener: AnimatorUpdateListener): FloatValueAnimatorBuilder {
        animator.addUpdateListener { animation -> listener.onUpdate(animation.animatedValue as Float) }
        return this
    }

    fun onEnd(endListener: AnimatorEndListener): FloatValueAnimatorBuilder {
        this.endListener = endListener
        return this
    }

    fun build(): ValueAnimator {
        animator.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                endListener?.onEnd()
            }
        })
        return animator
    }

}