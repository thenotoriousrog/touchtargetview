package com.thenotoriousrog.touchtargetview

import java.lang.reflect.AccessibleObject.setAccessible



class ReflectUtil {

    companion object {
        @Throws(NoSuchFieldException::class, IllegalAccessException::class)
        fun getPrivateField(source: Any, fieldName: String): Any {
            val objectField = source.javaClass.getDeclaredField(fieldName)
            objectField.isAccessible = true
            return objectField.get(source)
        }
    }
}