//package com.thenotoriousrog.touchtargetview
//
//import android.support.annotation.IdRes
//import android.support.v7.widget.Toolbar
//import android.text.TextUtils
//import android.view.View
//import java.io.FileDescriptor
//import java.io.ObjectInput
//import java.lang.IllegalStateException
//
////class ToolbarTouchTarget(toolbar: Toolbar, title: CharSequence, description: CharSequence?) : ViewTouchTarget(toolbar, title, description) {}
//class ToolbarTouchTarget : ViewTouchTarget {
//
//    constructor(toolbar: Toolbar, @IdRes menuItemId: Int, title: CharSequence, description: CharSequence?) : super(toolbar.findViewById(menuItemId), title, description)
//    constructor(toolbar: android.widget.Toolbar, @IdRes menuItemId: Int, title: CharSequence,  description: CharSequence?) : super(toolbar.findViewById(menuItemId), title, description)
//    constructor(toolbar: Toolbar, findNavView: Boolean, title: CharSequence, description: CharSequence?)  {
//        if(findNavView) {
//            findNavView(toolbar)
//        }
//        super(findNavView ? findNavView(toolbar) : findOverflowView(toolbar), title, description);
//    }
//    constructor(toolbar: android.widget.Toolbar, findNavView: Boolean, title: CharSequence, description: CharSequence?) : super(findNavView ? findNavView(toolbar) : findOverflowView(toolbar), title, description)
//
//    companion object {
//
//        private fun proxyOf(instance: Object): ToolbarProxy {
//            if(instance is Toolbar) {
//                return SupportToolbarProxy(instance as Toolbar)
//            } else if(instance is android.widget.Toolbar){ // todo: ignore these warnings of the toolbar thing. It's not a big deal because it will be converted into a library then it will be fixed!
//                return StandardToolbarProxy(instance as android.widget.Toolbar)
//            }
//
//            throw IllegalStateException("Couldn't provide proper toolbar proxy instance!")
//        }
//
//        private fun findNavView(instance: Object): View {
//            val toolbar = proxyOf(instance)
//
//            // attempt to find the view with a content description
//            val currentDescription = toolbar.getNavigationContentDescription()
//            val hadContentDescription = !TextUtils.isEmpty(currentDescription)
//            val sentinel = hadContentDescription ? currentDescription : ""
//            val sentinel = if (hadContentDescription) currentDescription else "taptarget-findme"
//        }
//
//    }
//
//}