package com.thenotoriousrog.touchtargetview

import android.content.Context
import android.graphics.Rect
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.support.annotation.ColorInt
import android.support.annotation.ColorRes
import android.support.annotation.DimenRes
import android.support.v7.widget.Toolbar
import android.support.annotation.IdRes
import android.support.v4.content.ContextCompat
import android.text.method.Touch
import android.util.Log
import android.view.ContextMenu
import android.view.View


/**
 * For creating the touch target that highlights standard Android views.
 */
open class TouchTarget(private var title: CharSequence, private var description: CharSequence?) {

    private val TAG = "TOUCH_TARGET_VIEW"
    var outerCircleAlpha = 0.96f
    var targetRadius = 44
    lateinit var bounds: Rect
    lateinit var icon: Drawable
    lateinit var titleTypeface: Typeface
    lateinit var descriptionTypeface: Typeface

    @ColorRes
    private var outerCircleColorRes = -1
    @ColorRes
    private var targetCircleColorRes = -1
    @ColorRes
    private var dimColorRes = -1
    @ColorRes
    private var titleTextColorRes = -1
    @ColorRes
    private var descriptionTextColorRes = -1

    private var outerCircleColor: Int? = null
    private var targetCircleColor: Int? = null
    private var dimColor: Int? = null
    private var titleTextColor: Int? = null
    private var descriptionTextColor: Int? = null

    @DimenRes
    private var titleTextDimen = -1
    @DimenRes
    private var descriptionTextDimen  = -1

    private var titleTextSize = 20
    private var descriptionTextSize = 20
    private var id = -1

    var drawShadow = false
    var cancelable = true
    var tintTarget = true
    var transparentTarget = false
    var descriptionTextAlpha = 0.54f

    constructor(bounds: Rect, title: CharSequence, description: CharSequence?): this(title, description) {
        this.bounds = bounds
    }


    companion object { // todo: do this last as it requires the most amount of work to get fixed.
//        /** Return a touch target for the navigation button (back, up, etc) from the given toolbar  */
//        fun forToolbarNavigationIcon(toolbar: Toolbar, title: CharSequence, description: CharSequence?): TouchTarget {
//            return ToolbarTouchTarget(toolbar, title, description)
//        }
//
//        /** Return a touch target for the menu item from the given toolbar  */
//        fun forToolbarMenuItem(
//            toolbar: Toolbar, @IdRes menuItemId: Int, title: CharSequence, description: CharSequence?): TouchTarget {
//            return ToolbarTouchTarget(toolbar, menuItemId, title, description)
//        }

        /* Returns a touch target for a view passed in from the user */
        fun forView(view: View, title: CharSequence, description: CharSequence?): TouchTarget {
            return ViewTouchTarget(view, title, description)
        }

        /* Returns a touch target for specified bounds */
        fun forBounds(bounds: Rect, title: CharSequence, description: CharSequence?): TouchTarget {
            return TouchTarget(bounds, title, description)
        }
    }

    /* Turns the target transparent */
    fun transparentTarget(transparent: Boolean): TouchTarget {
        this.transparentTarget = transparent
        return this
    }

    /* Specifies a color resource */
    fun outerCircleColor(@ColorRes color: Int): TouchTarget {
        this.outerCircleColorRes = color
        return this
    }

    /**
     * Specifies a color using an int
     */
    fun outerCircleColorInt(@ColorInt color: Int): TouchTarget {
        this.outerCircleColor = color
        return this
    }


    /* Controls the outer circle alpha from 0.0 to 1.0 */
    fun outerCircleAlpha(alpha: Float): TouchTarget {
        if(alpha < 0.0f) {
            this.outerCircleAlpha = 0.0f
            Log.w(TAG, "Alpha passed into TouchTargetView is below 0.0f. Setting outerCircleAlpha to 0.0f!")
        } else if(alpha > 1.0f) {
            this.outerCircleAlpha = 1.0f
            Log.w(TAG, "Alpha passed into TouchTargetView is above 1.0f. Setting outerCircleAlpha to 1.0f!")
        } else {
            this.outerCircleAlpha = alpha
        }
        return this
    }

    /* Changes the color of the target circle */
    fun targetCircleColor(@ColorRes color: Int): TouchTarget {
        this.targetCircleColorRes = color
        return this
    }

    /**
     * Changes the color of the title color using an int
     */
    fun targetCircleColorInt(@ColorInt color: Int): TouchTarget {
        this.targetCircleColor = color
        return this
    }

    /* sets the color for all text */
    fun textColor(@ColorRes color: Int): TouchTarget {
        this.titleTextColorRes = color
        this.descriptionTextColorRes = color
        return this
    }

    /**
     * sets a color using title text color
     */
    fun textColorInt(@ColorInt color: Int): TouchTarget {
        this.titleTextColor = color
        this.descriptionTextColor = color
        return this
    }

    /* Specify the color of the title text */
    fun titleTextColor(@ColorRes color: Int): TouchTarget {
        this.titleTextColor = color
        return this
    }

    /**
     * Specifies the color of the text using an int.
     */
    fun titleTextColorInt(@ColorInt color: Int): TouchTarget {
        this.titleTextColor = color
        return this
    }

    /* Specify the color of the description text */
    fun descriptionTextColor(@ColorRes color: Int): TouchTarget {
        this.descriptionTextColorRes = color
        return this
    }

    /**
     * Description text color using an int
     */
    fun descriptionTextColorInt(@ColorInt color: Int): TouchTarget {
        this.descriptionTextColor = color
        return this
    }

    /* Specify the typeface for the title text */
    fun titleTypeface(titleTypeface: Typeface): TouchTarget {
        this.titleTypeface = titleTypeface
        return this
    }

    /* Specify the typeface for the description text */
    fun descriptionTypeface(descriptionTypeface: Typeface): TouchTarget {
        this.descriptionTypeface = descriptionTypeface
        return this
    }

    /* Specifies the size of the title text */
    fun titleTextSize(sp: Int): TouchTarget {
        if(sp < 0) {
            Log.w(TAG, "title text size cannot be less than 0! Setting sp to be 0 instead!")
            this.titleTextSize = 0
        } else {
            titleTextSize = sp
        }
        return this
    }

    /* Specifies the size of the description text size */
    fun descriptionTextSize(sp: Int): TouchTarget {
        if(sp < 0) {
            Log.w(TAG, "description text size cannot be less than 0! Setting sp to be 0 instead!")
            this.descriptionTextSize = 0
        } else {
            this.descriptionTextSize = sp
        }
        return this
    }

    /**
     * Sets the title text size using a Dimen resource.
     * **Note: if this is used it will override whatever is passed in using sp size!
     */
    fun titleTextDimen(@DimenRes dimen: Int): TouchTarget {
        this.titleTextDimen = dimen
        return this
    }

    /**
     * Sets the description text size using a Dimen resource.
     * **Note: if this is used it will override whatever is passed in using sp size!
     */
    fun descriptionTextDimen(@DimenRes dimen: Int): TouchTarget {
        this.descriptionTextDimen = dimen
        return this
    }

    /* Specifies the alpha for the description text */
    fun descriptionTextAlpha(descriptionTextAlpha: Float): TouchTarget {
        if(descriptionTextAlpha < 0.0f) {
            Log.w(TAG, "Description text alpha cannot be less than 0.0f! Setting alpha to 0.0f instead!")
            this.descriptionTextAlpha = 0.0f
        } else if(descriptionTextAlpha > 1.0f) {
            Log.w(TAG, "Description text alpha cannot be greater than 1.0f! Setting alpha to 1.0f instead!")
            this.descriptionTextAlpha = 1.0f
        } else {
            this.descriptionTextAlpha = descriptionTextAlpha
        }
        return this
    }

    /**
     * This will specify the color to use as a dimming effect.
     * **Note: The given color will have it's alpha modified by roughly 30%
     */
    fun dimColor(@ColorRes color: Int): TouchTarget {
        this.dimColorRes = color
        return this
    }

    fun dimColorInt(@ColorInt color: Int): TouchTarget {
        this.dimColor = color
        return this
    }

    /* Tells TouchTarget to drop a shadow around the outer circle or not */
    fun drawShadow(shadow: Boolean): TouchTarget {
        this.drawShadow = shadow
        return this
    }

    /* Tells TouchTarget whether or not the target can be cancelable or not */
    fun cancelable(status: Boolean): TouchTarget {
        this.cancelable = status
        return this
    }

    /**
     * Tells touch target whether or not to tint the target's icon with the outer circle's color
     */
    fun tintTarget(tint: Boolean): TouchTarget {
        this.tintTarget = tint
        return this
    }

    /**
     * Gives the icon that will be drawn in the center of the target bounds.
     */
    fun icon(icon: Drawable): TouchTarget {
        return icon(icon, false)
    }

    /**
     * Gives the icon that will be drawn int the center of the target bounds.
     * @param hasSetBounds - Whether the drawable already has it's bounds correctly set. If the drawable does not have its bounds set, then place it in the center.
     */
    fun icon(icon: Drawable, hasSetBounds: Boolean): TouchTarget {
        this.icon = icon
        if(!hasSetBounds) {
            this.icon.bounds = Rect(0,0,this.icon.intrinsicWidth, this.icon.intrinsicWidth)
        }
        return this
    }

    /**
     * Gives a unique id for this target
     */
    fun id(id: Int): TouchTarget {
        this.id = id
        return this
    }

    /**
     * Specifies the radius in dp that the target should be
     */
    fun targetRadius(targetRadius: Int): TouchTarget {
        this.targetRadius = targetRadius
        return this
    }

    /**
     * Returns the id for this TouchTarget
     */
    fun getId(): Int {
        return this.id
    }

    /**
     * In case the target needs time to be ready (for reasons like items laid out in the view, not created, or other reasons),
     * the runnable can be passed here and will be invoked when the target is ready!
     */
    open fun onReady(runnable: Runnable) {
        runnable.run()
    }

    /**
     * Returns the target bounds.
     * NOTE: the target may not be ready in all cases.
     * Called internally when onReady() invokes its runnable
     */
//    fun getBound(): Rect {
//        return bounds
//    }

    private fun getColorCompat(context: Context, value: Int?, @ColorRes resource: Int): Int? {
        if(resource != -1) {
            return ContextCompat.getColor(context, resource)
        }

        return value
    }

    private fun getSize(context: Context, size: Int, @DimenRes dimen: Int): Int {
        if(dimen != -1) {
            return context.resources.getDimensionPixelSize(dimen)
        }
        return UiUtil.sp(context, size.toFloat())
    }

    /**
     * Returns res or int version of the outerCircleColor
     */
    fun getOuterCircleColor(context: Context): Int? {
        return getColorCompat(context, outerCircleColor, outerCircleColorRes)
    }

    /**
     * Returns the res or int color of the targetCircleColor
     */
    fun getTargetCircleColor(context: Context): Int? {
        return getColorCompat(context, targetCircleColor, targetCircleColorRes)
    }

    /**
     * Returns the res or int color of the dimColor
     */
    fun getDimColor(context: Context): Int? {
        return getColorCompat(context, dimColor, dimColorRes)
    }

    /**
     * returns the res or int color of the title text color
     */
    fun getTitleTextColor(context: Context): Int? {
        return getColorCompat(context, titleTextColor, titleTextColorRes)
    }

    /**
     * Returns the res or int color of the description color
     */
    fun getDescriptionTextColor(context: Context): Int? {
        return getColorCompat(context, descriptionTextColor, descriptionTextColorRes)
    }

    /**
     * Returns the title size in dimen or int
     */
    fun getTitleTextSizePx(context: Context): Int {
        return getSize(context, titleTextSize, titleTextDimen)
    }

    /**
     * Returns the description size in dimen or int
     */
    fun descriptionTextSizePx(context: Context): Int {
        return getSize(context, descriptionTextSize, descriptionTextDimen)
    }

    /**
     * Returns the title of the TouchTarget
     */
    fun getTitle(): CharSequence {
        return title
    }

    /**
     * Returns a nullable description of TouchTarget
     */
    fun getDescription(): CharSequence? {
        return description
    }
}