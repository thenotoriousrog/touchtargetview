package com.thenotoriousrog.touchtargetview

import android.animation.TypeEvaluator
import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Outline
import android.graphics.Paint
import android.graphics.Path
import android.graphics.PixelFormat
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.graphics.PorterDuffXfermode
import android.graphics.Rect
import android.graphics.Region
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.os.Build
import android.renderscript.Sampler
import android.text.DynamicLayout
import android.text.Layout
import android.text.SpannableStringBuilder
import android.text.StaticLayout
import android.text.TextPaint
import android.text.method.Touch
import android.util.DisplayMetrics
import android.view.Gravity
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.ViewManager
import android.view.ViewOutlineProvider
import android.view.ViewTreeObserver
import android.view.WindowManager
import android.view.animation.AccelerateDecelerateInterpolator


/**
 * TouchTargetView creates and view that showcases a feature within an app that follows material design.
 * This class should not be instantiated directly. It should be use with the showFor(Activity, TouchTarget, Listener) static factory method instead.
 */
class TouchTargetView(
    context: Context, private val parent: ViewManager, private val boundingParent: ViewGroup?,
    private val target: TouchTarget, private val userListener: TouchTargetViewListener
) : View(context) {

    private var isDismissed = false
    private var isDimissing = false
    private var isInteractable = true

    val TARGET_PADDING: Int
    val TARGET_RADIUS: Int
    val TARGET_PULSE_RADIUS: Int
    val TEXT_PADDING: Int
    val TEXT_SPACING: Int
    val TEXT_MAX_WIDTH: Int
    val TEXT_POSITIONING_BIAS: Int
    val CIRCLE_PADDING: Int
    val GUTTER_DIM: Int
    val SHADOW_DIM: Int
    val SHADOW_JITTER_DIM: Int

    val targetBounds: Rect

    val titlePaint: TextPaint
    val descriptionPaint: TextPaint
    val outerCirclePaint: Paint
    val outerCircleShadowPaint: Paint
    val targetCirclePaint: Paint
    val targetCirclePulsePaint: Paint

    val title: CharSequence
    var titleLayout: StaticLayout?
    val description: CharSequence?
    var descriptionLayout: StaticLayout?

    var isDark: Boolean
    var debug: Boolean
    var shouldTintTarget: Boolean
    var shouldDrawShadow: Boolean
    var cancelable: Boolean
    var visible: Boolean

    // Debug related variables
    var debugStringBuilder: SpannableStringBuilder?
    var debugLayout: DynamicLayout?
    var debugTextPaint: TextPaint?
    var debugPaint: Paint?

    // Drawing variables
    var drawingBounds: Rect
    var textBounds: Rect

    var outerCirclePath: Path
    var outerCircleRadius: Float
    var calculatedOuterCircleRadius: Int
    var outerCircleCenter: FloatArray
    var outerCircleAlpha: Int
    var targetCirclePulseRadius: Float
    var targetCirclePulseAlpha: Int
    var targetCircleRadius: Float
    var targetCircleAlpha: Int
    var textAlpha: Int
    var dimColor: Int
    var lastTouchX: Float
    var lastTouchY: Float
    var topBoundary: Int
    var bottomBoundary: Int
    var tintedTarget: Bitmap?
    var listener: TouchTargetViewListener

    var outlineProvider: ViewOutlineProvider?

    companion object {
        fun showFor(activity: Activity, target: TouchTarget): TouchTargetView {
            return showFor(activity, target, null)
        }

        fun showFor(activity: Activity, target: TouchTarget, listener: TouchTargetViewListener): TouchTargetView {
            val decor = activity.window.decorView as ViewGroup
            val layoutParams =
                ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            val content = decor.findViewById<ViewGroup>(android.R.id.content)
            val touchTargetView = TouchTargetView(activity, decor, content, target, listener)
            decor.addView(touchTargetView)
            return touchTargetView
        }

        fun showFor(dialog: Dialog, target: TouchTarget): TouchTargetView {
            return showFor(dialog, target, null)
        }

        fun showFor(dialog: Dialog, target: TouchTarget, listener: TouchTargetViewListener): TouchTargetView {
            val context = dialog.context
            val windowManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            val params = WindowManager.LayoutParams()
            params.type = WindowManager.LayoutParams.TYPE_APPLICATION
            params.format = PixelFormat.RGBA_8888
            params.flags = 0
            params.gravity = Gravity.START or Gravity.TOP
            params.x = 0
            params.y = 0
            params.width = WindowManager.LayoutParams.MATCH_PARENT
            params.height = WindowManager.LayoutParams.MATCH_PARENT

            val touchTargetView = TouchTargetView(context, windowManager, null, target, listener)
            windowManager.addView(touchTargetView, params)

            return touchTargetView
        }
    }

    val expandContractUpdateListener = object : FloatValueAnimatorBuilder.AnimatorUpdateListener {
        override fun onUpdate(lerpTime: Float) {
            val newOuterCircleRadius = calculatedOuterCircleRadius * lerpTime
            val expanding = newOuterCircleRadius > outerCircleRadius
            if (!expanding) {
                // we need to ensure that we are invalidating the old drawing bounds while contracting. We don't want ghost images showing up.
                calculateDrawingBounds()
            }
            val targetAlpha = target.outerCircleAlpha * 255
            outerCircleRadius = newOuterCircleRadius
            outerCircleAlpha = Math.min(targetAlpha, (lerpTime * 1.5f * targetAlpha)).toInt()
            outerCirclePath.reset()
            outerCirclePath.addCircle(outerCircleCenter[0], outerCircleCenter[1], outerCircleRadius, Path.Direction.CW)

            targetCircleAlpha = Math.min(255.0f, (lerpTime * 1.5f * 255.0f)).toInt()
            if (expanding) {
                targetCircleRadius = TARGET_RADIUS * Math.min(1.0f, lerpTime * 1.5f)
            } else {
                targetCircleRadius = TARGET_RADIUS * lerpTime
                targetCirclePulseRadius *= lerpTime
            }

            textAlpha = (delayedLerp(lerpTime, 0.7f) * 255).toInt()

            if (expanding) {
                calculateDrawingBounds()
            }

            invalidateViewAndOutline(drawingBounds)
        }
    }

    val expandAnimation: ValueAnimator = FloatValueAnimatorBuilder()
        .duration(250)
        .delayBy(250)
        .interpolator(AccelerateDecelerateInterpolator())
        .onUpdate(object : FloatValueAnimatorBuilder.AnimatorUpdateListener {
            override fun onUpdate(lerpTime: Float) {
                expandContractUpdateListener.onUpdate(lerpTime)
            }
        })
        .onEnd(object : FloatValueAnimatorBuilder.AnimatorEndListener {
            override fun onEnd() {
                // todo: check line 275 in TouchTargetView to continue progress on the application!
                pulseAnimation.start()
                isInteractable = true
            }
        })
        .build()

    val pulseAnimation = FloatValueAnimatorBuilder()
        .duration(1000)
        .repeat(ValueAnimator.INFINITE)
        .interpolator(AccelerateDecelerateInterpolator())
        .onUpdate(object : FloatValueAnimatorBuilder.AnimatorUpdateListener {
            override fun onUpdate(lerpTime: Float) {
                val pulseLerp = delayedLerp(lerpTime, 0.5f)
                targetCirclePulseRadius = (1.0f - pulseLerp) * TARGET_RADIUS
                targetCirclePulseAlpha = ((1.0f - pulseLerp) * 255).toInt()
                targetCircleRadius = TARGET_RADIUS + halfwayLerp(lerpTime) * TARGET_PULSE_RADIUS

                if (outerCirlceRadius != calculatedOuterCircleRadius) {
                    outerCircleRadius = calculatedOuterCircleRadius
                }

                calculateDrawingBounds()
                invalidateViewAndOutline(drawingBounds)
            }
        })
        .build()

    val dismissAnimation =
        FloatValueAnimatorBuilder() // the original one sends in "true" into FloatValueAnimatorBuilder instead of me calling "reverse(true)"
            .reverse(true)
            .duration(250)
            .interpolator(AccelerateDecelerateInterpolator())
            .onUpdate(object : FloatValueAnimatorBuilder.AnimatorUpdateListener {
                override fun onUpdate(lerpTime: Float) {
                    expandContractUpdateListener.onUpdate(lerpTime)
                }
            })
            .onEnd(object : FloatValueAnimatorBuilder.AnimatorEndListener {
                override fun onEnd() {
                    finishDismiss(true)
                }
            })
            .build()

    private val dismissConfirmAnimation = FloatValueAnimatorBuilder()
        .duration(250)
        .interpolator(AccelerateDecelerateInterpolator())
        .onUpdate(object : FloatValueAnimatorBuilder.AnimatorUpdateListener {
            override fun onUpdate(lerpTime: Float) {
                val spedUpLerp = Math.min(1.0f, lerpTime * 2.0f)
                outerCircleRadius = calculatedOuterCircleRadius * (1.0f + (spedUpLerp * 0.2f))
                outerCircleAlpha = ((1.0f - spedUpLerp) * target.outerCircleAlpha * 255.0f).toInt()
                outerCirclePath.reset()
                outerCirclePath.addCircle(
                    outerCircleCenter[0],
                    outerCircleCenter[1],
                    outerCircleRadius,
                    Path.Direction.CW
                ) // figure out what path.Direction.CW does
                targetCircleRadius = (1.0f - lerpTime) * TARGET_RADIUS
                targetCircleAlpha = ((1.0f - lerpTime) * 255.0f).toInt()
                targetCirclePulseRadius = (1.0f + lerpTime) * TARGET_RADIUS
                targetCirclePulseAlpha = ((1.0f - lerpTime) * TARGET_RADIUS).toInt()
                textAlpha = ((1.0f - spedUpLerp) * 255.0f).toInt()
                calculateDrawingBounds()
                invalidateViewAndOutline(drawingBounds)
            }
        })
        .onEnd(object : FloatValueAnimatorBuilder.AnimatorEndListener {
            override fun onEnd() {
                finishDismiss(true)
            }
        })
        .build()

    private val animators = arrayOf(expandAnimation, pulseAnimation, dismissConfirmAnimation, dismissAnimation)
    private val globalLayoutListener: ViewTreeObserver.OnGlobalLayoutListener

    init {
        this.listener = userListener
        this.title = target.getTitle()
        this.description = target.getDescription()

        TARGET_PADDING = UiUtil.dp(context, 20.0f)
        CIRCLE_PADDING = UiUtil.dp(context, 40.0f)
        TARGET_RADIUS = UiUtil.dp(context, target.targetRadius.toFloat())
        TEXT_PADDING = UiUtil.dp(context, 40.0f)
        TEXT_SPACING = UiUtil.dp(context, 8.0f)
        TEXT_MAX_WIDTH = UiUtil.dp(context, 360.0f)
        TEXT_POSITIONING_BIAS = UiUtil.dp(context, 20.0f)
        GUTTER_DIM = UiUtil.dp(context, 88.0f)
        SHADOW_DIM = UiUtil.dp(context, 8.0f)
        SHADOW_JITTER_DIM = UiUtil.dp(context, 1.0f)
        TARGET_PULSE_RADIUS = (0.1f * TARGET_RADIUS).toInt()

        this.outerCirclePath = Path()
        this.targetBounds = Rect()
        this.drawingBounds = Rect()

        this.titlePaint = TextPaint()
        this.titlePaint.textSize = target.getTitleTextSizePx(context).toFloat()
        this.titlePaint.typeface = Typeface.create("sans-serif-medium", Typeface.NORMAL)
        this.titlePaint.isAntiAlias = true

        this.descriptionPaint = TextPaint()
        this.descriptionPaint.textSize = target.descriptionTextSizePx(context).toFloat()
        this.descriptionPaint.typeface = Typeface.create(Typeface.SANS_SERIF, Typeface.NORMAL)
        this.descriptionPaint.isAntiAlias = true
        this.descriptionPaint.alpha = (0.54f * 255.0f).toInt()

        this.outerCirclePaint = Paint()
        this.outerCirclePaint.isAntiAlias = true
        this.outerCirclePaint.alpha = (target.outerCircleAlpha * 255.0f).toInt()

        this.outerCircleShadowPaint = Paint()
        this.outerCircleShadowPaint.isAntiAlias = true
        this.outerCircleShadowPaint.alpha = 50
        this.outerCircleShadowPaint.style = Paint.Style.STROKE
        this.outerCircleShadowPaint.strokeWidth = SHADOW_JITTER_DIM.toFloat()
        this.outerCircleShadowPaint.color = Color.BLACK

        this.targetCirclePaint = Paint()
        this.targetCirclePaint.isAntiAlias = true

        this.targetCirclePulsePaint = Paint()
        this.targetCirclePulsePaint.isAntiAlias = true

        applyTargetOption(context)

        val hasKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT
        val translucentStatusBar: Boolean
        val translucentNavigationBar: Boolean
        val layoutNoLimits: Boolean

        if (context is Activity) {
            val activity = context as Activity
            val flags = activity.window.attributes.flags
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                translucentStatusBar = ((flags and WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS) != 0)
                translucentNavigationBar = ((flags and WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION) != 0)
                layoutNoLimits = ((flags and WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS) != 0)
            } else { // if doesn't have KitKat, set to false no matter what
                translucentStatusBar = false
                translucentNavigationBar = false
                layoutNoLimits = false
            }
        } else {
            translucentStatusBar = false
            translucentNavigationBar = false
            layoutNoLimits = false
        }

        this.globalLayoutListener = object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                if (isDimissing) {
                    return
                }

                updateTextLayouts()
                target.onReady(Runnable {

                    val offset = IntArray(2)
                    targetBounds.set(target.bounds)

                    getLocationOnScreen(offset)
                    targetBounds.offset(-offset[0], -offset[1])

                    if (boundingParent != null) {
                        val windowManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
                        val displayMetrics = DisplayMetrics()
                        windowManager.defaultDisplay.getMetrics(displayMetrics)

                        val rect = Rect()
                        boundingParent.getWindowVisibleDisplayFrame(rect)
                        val parentLocation = IntArray(2)
                        boundingParent.getLocationInWindow(parentLocation)

                        if (translucentStatusBar) {
                            rect.top = parentLocation[1]
                        }
                        if (translucentNavigationBar) {
                            rect.bottom = parentLocation[1] + boundingParent.height
                        }

                        // We must bound the boundaries to be within the screen's coordinates to handle the case where the flag FLAG_LAYOUT_NO_LIMITS is set
                        if (layoutNoLimits) {
                            topBoundary = Math.max(0, rect.top)
                            bottomBoundary = Math.min(rect.bottom, displayMetrics.heightPixels)
                        } else {
                            topBoundary = rect.top
                            bottomBoundary = rect.bottom
                        }
                    }

                    drawTintedTarget()
                    requestFocus()
                    calculateDimensions()

                    startExpandAnimation()

                })
            }
        }

        this.viewTreeObserver.addOnGlobalLayoutListener(globalLayoutListener)

        this.isFocusableInTouchMode = true
        this.isClickable = true
        this.setOnClickListener {

            val clickedInTarget = distance(
                targetBounds.centerX(),
                targetBounds.centerY(),
                lastTouchX.toInt(),
                lastTouchY.toInt()
            ) <= targetCircleRadius
            val distanceToOuterCircleCenter =
                distance(outerCircleCenter[0], outerCircleCenter[1], lastTouchX.toInt(), lastTouchY.toInt())
            val clickedInsideOfOuterCircle = distanceToOuterCircleCenter <= outerCircleRadius

            if (clickedInTarget) {
                isInteractable = false
                listener.onTargetClick(this@TouchTargetView)
            } else if (clickedInsideOfOuterCircle) {
                listener.onOuterCircleClick(this@TouchTargetView)
            } else if (cancelable) {
                isInteractable = false
                listener.onTargetCancel(this@TouchTargetView)
            }
        }

        this.setOnLongClickListener(object : View.OnLongClickListener {

            override fun onLongClick(p0: View?): Boolean {
                if (targetBounds.contains(lastTouchX.toInt(), lastTouchY.toInt())) {
                    listener.onTargetLongClick(this@TouchTargetView)
                    return true
                }
                return false
            }
        })
    }

    private fun startExpandAnimation() {
        if (!visible) {
            isInteractable = false
            expandAnimation.start()
            visible = true
        }
    }

    protected fun applyTargetOption(context: Context) {
        shouldTintTarget = !target.transparentTarget && target.tintTarget
        shouldDrawShadow = target.drawShadow
        cancelable = target.cancelable

        // We can't clip out portions of the view outline, so if the user specified a transparent target, we need to fallback to
        // drawing a jittered shadow approximation
        if (shouldDrawShadow && Build.VERSION.SDK_INT >= 21 && !target.transparentTarget) {
            outlineProvider = object : ViewOutlineProvider() {

                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                override fun getOutline(view: View?, outline: Outline?) {
                    outline?.setOval(
                        (outerCircleCenter[0] - outerCircleRadius).toInt(),
                        (outerCircleCenter[1] - outerCircleRadius).toInt(),
                        (outerCircleCenter[0] + outerCircleRadius).toInt(),
                        (outerCircleCenter[1] + outerCircleRadius).toInt()
                    )

                    outline?.alpha = outerCircleAlpha / 255.0f
                    if (Build.VERSION.SDK_INT >= 22) {
                        outline?.offset(0, SHADOW_DIM)
                    }
                }
            }

            setOutlineProvider(outlineProvider)
            elevation = SHADOW_DIM.toFloat()
        }

        if (shouldDrawShadow && outlineProvider == null && Build.VERSION.SDK_INT < 18) {
            setLayerType(LAYER_TYPE_SOFTWARE, null)
        } else {
            setLayerType(LAYER_TYPE_HARDWARE, null)
        }

        val theme = context.theme
        isDark = UiUtil.themeIntAttr(context, "isLightTheme") == 0
        val outerCircleColor = target.getOuterCircleColor(context)
        when {
            outerCircleColor != null -> outerCirclePaint.color = outerCircleColor
            theme != null -> outerCirclePaint.color = UiUtil.themeIntAttr(context, "colorPrimary")
            else -> outerCirclePaint.color = Color.WHITE
        }

        val targetCircleColor = target.getTargetCircleColor(context)
        if (targetCircleColor != null) {
            targetCirclePaint.color = targetCircleColor
        } else {
            if (isDark) {
                targetCirclePaint.color = Color.BLACK
            } else {
                targetCirclePaint.color = Color.WHITE
            }
        }

        if (target.transparentTarget) {
            targetCirclePaint.xfermode = PorterDuffXfermode(PorterDuff.Mode.CLEAR)
        }

        targetCirclePulsePaint.color = targetCirclePaint.color

        val targetDimColor = target.getDimColor(context)
        if (targetDimColor != null) {
            dimColor = UiUtil.setAlpha(targetDimColor, 0.3f)
        } else {
            dimColor = -1
        }

        val titleTextColor = target.getDimColor(context)
        if (titleTextColor != null) {
            titlePaint.color = titleTextColor
        } else {
            if (isDark) {
                titlePaint.color = Color.BLACK
            } else {
                titlePaint.color = Color.WHITE
            }
        }

        val descriptionTextColor = target.getDescriptionTextColor(context)
        if (descriptionTextColor != null) {
            descriptionPaint.color = descriptionTextColor
        } else {
            descriptionPaint.color = titlePaint.color
        }

        titlePaint.typeface = target.titleTypeface
        descriptionPaint.typeface = target.descriptionTypeface
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        onDismiss(false)
    }

    fun onDismiss(userInitiated: Boolean) {
        if (isDismissed) {
            return
        }

        isDimissing = false
        isDismissed = true

        for (animator: ValueAnimator in animators) {
            animator.cancel()
            animator.removeAllUpdateListeners()
        }

        ViewUtil.removeOnGlobalLayoutListener(viewTreeObserver, globalLayoutListener)
        visible = false

        listener.onTargetDismissed(this, userInitiated)
    }

    override fun onDraw(canvas: Canvas?) {

        if (isDismissed) {
            return
        }

        if (topBoundary > 0 && bottomBoundary > 0) {
            canvas?.clipRect(0, topBoundary, width, bottomBoundary)
        }

        if (dimColor != -1) {
            canvas?.drawCircle(dimColor)
        }

        var saveCount: Int?
        outerCirclePaint.alpha = outerCircleAlpha

        if (shouldDrawShadow && outlineProvider == null) {
            saveCount = canvas?.save()
            run {
                // **Note: this method is deprecated, but only for invalid Region.Op Values.
                // Region.Op.DIFFERENCE and Region.Op.INTERSECT are the only valid Region.Op values as of Build.VERSION_CODES.P
                canvas?.clipPath(outerCirclePath, Region.Op.DIFFERENCE)
                drawJitteredShadow(canvas)
            }
            if (saveCount == null) {
                canvas?.restoreToCount(0)
            } else {
                canvas?.restoreToCount(saveCount)
            }
        }

        canvas?.drawCircle(outerCircleCenter[0], outerCircleCenter[1], outerCircleRadius, outerCirclePaint)

        targetCirclePaint.alpha = targetCircleAlpha
        if (targetCirclePulseAlpha > 0) {
            targetCirclePulsePaint.alpha = targetCirclePulseAlpha
            canvas?.drawCircle(
                targetBounds.centerX().toFloat(),
                targetBounds.centerY().toFloat(),
                targetCirclePulseRadius,
                targetCirclePulsePaint
            )
        }

        saveCount = canvas?.save()
        run {
            canvas?.translate(textBounds.left.toFloat(), textBounds.top.toFloat())
            titlePaint.alpha = textAlpha
            if (titleLayout != null) {
                titleLayout.draw(canvas)
            }

            if (descriptionLayout != null && titleLayout != null) {
                canvas?.translate(0f, (titleLayout.height + TEXT_SPACING).toFloat())
                descriptionPaint.alpha = (target.descriptionTextAlpha * textAlpha).toInt()
                descriptionLayout.draw(canvas)
            }
        }
        if (saveCount == null) {
            canvas?.restoreToCount(0)
        } else {
            canvas?.restoreToCount(saveCount)
        }

        saveCount = canvas?.save()
        run {
            canvas?.translate(
                (targetBounds.centerX() - tintedTarget!!.width / 2).toFloat(),
                (targetBounds.centerY() - tintedTarget!!.width / 2).toFloat()
            )
            canvas?.drawBitmap(tintedTarget!!, 0f, 0f, targetCirclePaint)

            canvas?.translate(
                (targetBounds.centerX() - target.icon.bounds.width() / 2).toFloat(),
                (targetBounds.centerY() - target.icon.bounds.height() / 2).toFloat()
            )
            target.icon.alpha = targetCirclePaint.alpha
            target.icon.draw(canvas)
        }

        if (saveCount == null) {
            canvas?.restoreToCount(0)
        } else {
            canvas?.restoreToCount(saveCount)
        }

        if (debug) {
            drawDebugInformation(canvas)
        }
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        lastTouchX = event.x
        lastTouchY = event.y
        return super.onTouchEvent(event)
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if(visible && cancelable && keyCode == KeyEvent.KEYCODE_BACK) {
            event?.startTracking()
            return true
        }
        return false
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent): Boolean {

        if(visible && isInteractable && cancelable && keyCode == KeyEvent.KEYCODE_BACK && event.isTracking && !event.isCanceled) {
            isInteractable = false

            listener.onTargetCancel(this@TouchTargetView)
            return true
        }
        return false
    }

    /**
     * Dismiss this view
     * @param tappedTarget - If the user tapped the target or not (this creates a different dismiss animation)
     */
    fun dismiss(tappedTarget: Boolean) {
        isDimissing = true
        pulseAnimation.cancel()
        expandAnimation.cancel()
        if(!visible) {
            finishDismiss(tappedTarget)
            return
        }

        if(tappedTarget) {
            dismissConfirmAnimation.start()
        } else {
            dismissAnimation.start()
        }
    }

    private fun finishDismiss(userInitiated: Boolean) {
        onDismiss(userInitiated)
        ViewUtil.removeView(parent, this@TouchTargetView)
    }

    // Decide whether or not to draw a wireframe around the view, super helpful for debugging
    fun setDrawDebug(status: Boolean) {
        if(debug != status) {
            debug = status
            postInvalidate()
        }
    }

    /**
     * Returns whether or not this view is visible
     */
    fun isVisible(): Boolean {
        return !isDismissed && visible
    }

    fun drawJitteredShadow(canvas: Canvas?) {
        val baseAlpha = 0.20f * outerCircleAlpha
        outerCircleShadowPaint.style = Paint.Style.FILL_AND_STROKE
        outerCircleShadowPaint.alpha = baseAlpha.toInt()
        canvas?.drawCircle(outerCircleCenter[0], outerCircleCenter[1] + SHADOW_DIM, outerCircleRadius, outerCircleShadowPaint)
        val numJitters = 7

        for(i in numJitters-1 downTo 0) {
            outerCircleShadowPaint.alpha = ((i/numJitters) * baseAlpha).toInt()
            canvas?.drawCircle(outerCircleCenter[0], outerCircleCenter[1] + SHADOW_DIM, outerCircleRadius + (numJitters - i) * SHADOW_JITTER_DIM, outerCircleShadowPaint)
        }
    }

    fun drawDebugInformation(canvas: Canvas?) {
        if(debugPaint == null) {
            debugPaint = Paint()
            debugPaint?.setARGB(255,255,0,0)
            debugPaint?.style = Paint.Style.STROKE
            debugPaint?.strokeWidth = UiUtil.dp(context, 1.0f).toFloat()
        }

        if(debugTextPaint == null) {
            debugTextPaint = TextPaint()
            debugTextPaint?.color = 0xFFFF0000.toInt()
            debugTextPaint?.textSize = UiUtil.sp(context, 16.0f).toFloat()
        }

        // Draw wireframe
        debugPaint?.style = Paint.Style.STROKE
        canvas?.drawRect(textBounds, debugPaint)
        canvas?.drawRect(targetBounds, debugPaint)
        canvas?.drawCircle(outerCircleCenter[0], outerCircleCenter[1], 10.0f, debugPaint)
        canvas?.drawCircle(outerCircleCenter[0], outerCircleCenter[1], (calculatedOuterCircleRadius - CIRCLE_PADDING).toFloat(), debugPaint)
        canvas?.drawCircle(targetBounds.centerX().toFloat(), targetBounds.centerY().toFloat(), (TARGET_RADIUS + TARGET_PADDING).toFloat(), debugPaint)

        // Draw positions and dimensions
        debugPaint?.style = Paint.Style.FILL

        val debugText =
            "Text bounds: " + textBounds.toShortString() + "\n" +
                    "Target bounds: " + targetBounds.toShortString() + "\n" +
                    "Center: " + outerCircleCenter[0] + " " + outerCircleCenter[1] + "\n" +
                    "View size: " + width + " " + height + "\n" +
                    "Target bounds: " + targetBounds.toShortString()

        // **Note: DynamicLayout, at least with the constructor I'm using, is deprecated. Debug mode is very rarely going to be used anyways and isn't meant to be a final feature.
        if(debugLayout == null) {
            debugLayout = DynamicLayout(debugText, debugTextPaint, width, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false)
        }

        val saveCount = canvas?.save()
        run {

            if(debugLayout != null) {
                debugPaint?.setARGB(220, 0, 0, 0)
                canvas?.translate(0.0f, topBoundary.toFloat())
                canvas?.drawRect(0.0f, 0.0f, debugLayout!!.width.toFloat(), debugLayout!!.height.toFloat(), debugPaint)
                debugPaint?.setARGB(255,255,0,0)
                debugLayout?.draw(canvas)
            }
        }

        if(saveCount == null) {
            canvas?.restoreToCount(0)
        }
        else {
            canvas.restoreToCount(saveCount)
        }
    }

    fun drawTintedTarget() {
        val icon = target.icon
        if(!shouldTintTarget) {
            tintedTarget = null
            return
        }

        if(tintedTarget != null) {
            return
        }

        tintedTarget = Bitmap.createBitmap(icon.intrinsicWidth, icon.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(tintedTarget!!)
        icon.colorFilter = PorterDuffColorFilter(outerCirclePaint.color, PorterDuff.Mode.SRC_ATOP)
        icon.draw(canvas)
        icon.colorFilter = null
    }

    fun updateTextLayouts() {
        val textWidth = Math.min(width, TEXT_MAX_WIDTH) - (TEXT_PADDING * 2)
        if(textWidth <= 0) {
            return
        }

        // todo: need to find another layout besides a static layout to achieve a better design for our views. To make it more flexible
        titleLayout = StaticLayout(title, titlePaint, textWidth, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false)

        if(description != null) {
            descriptionLayout = StaticLayout(description, descriptionPaint, textWidth, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false)
        } else {
            descriptionLayout = null
        }
    }

    fun halfwayLerp(lerp: Float): Float {
        if(lerp < 0.5f) {
            return lerp / 0.5f
        }

        return (1.0f - lerp) / 0.5f
    }

    fun delayedLerp(lerp: Float, threshold: Float): Float {
        if(lerp < threshold) {
            return 0.0f
        }

        return (lerp - threshold) / (1.0f - threshold)
    }

    fun calculateDimensions() {
        textBounds = getTextBounds()
        outerCircleCenter = getOuterCircleCenterPoint()
        calculatedOuterCircleRadius = getOuterCircleRadius(outerCircleCenter[0], outerCircleCenter[1], textBounds, targetBounds)
    }

    fun calculateDrawingBounds() {
        drawingBounds.left = Math.max(0.0f, outerCircleCenter[0] - outerCircleRadius).toInt()
        drawingBounds.top = Math.min(0.0f, outerCircleCenter[1] - outerCircleRadius).toInt()
        drawingBounds.right = Math.min(width.toFloat(), outerCircleCenter[0] + outerCircleRadius + CIRCLE_PADDING).toInt()
        drawingBounds.bottom = Math.min(height.toFloat(), outerCircleCenter[1] + outerCircleRadius + CIRCLE_PADDING).toInt()
    }

    fun getOuterCircleRadius(centerX: Int, centerY: Int, textBounds: Rect, targetBounds: Rect): Int {
        val targetCenterX = targetBounds.centerX()
        val targetCenterY = targetBounds.centerY()
        val expandedRadius = (1.1f * TARGET_RADIUS).toInt()
        val expandedBounds = Rect(targetCenterX, targetCenterY, targetCenterX, targetCenterY)
        expandedBounds.inset(-expandedRadius, -expandedRadius)

        val textRadius = maxDistanceToPoints(centerX, centerY, textBounds)
        val targetRadius = maxDistanceToPoints(centerX, centerY, expandedBounds)
        return Math.max(textRadius, targetRadius) + CIRCLE_PADDING
    }

    fun getTextBounds(): Rect {

        val totalTextHeight = getTotalTextHeight()
        val totalTextWidth = getTotalTextWidth()

        val possibleTop = targetBounds.centerY() - TARGET_RADIUS - TARGET_PADDING - totalTextHeight
        val top: Int
        if(possibleTop > topBoundary) {
            top = possibleTop
        } else {
            top = targetBounds.centerY() + TARGET_RADIUS + TARGET_PADDING
        }

        val relativeCenterDistance = (width / 2) - targetBounds.centerX()
        val bias: Int
        if(relativeCenterDistance < 0) {
            bias = -TEXT_POSITIONING_BIAS
        } else {
            bias = TEXT_POSITIONING_BIAS
        }

        val left = Math.max(TEXT_PADDING, targetBounds.centerX() - bias - totalTextWidth)
        val right = Math.min(width - TEXT_PADDING, left + totalTextWidth)
        return Rect(left, top, right, top + totalTextHeight)
    }

    fun getOuterCircleCenterPoint(): IntArray {

        if(inGutter(targetBounds.centerY())) {
            return intArrayOf(targetBounds.centerX(), targetBounds.centerY())
        }

        val targetRadius = (Math.max(targetBounds.width(), targetBounds.height()) / 2) + TARGET_PADDING
        val totalTextHeight = getTotalTextHeight()

        val onTop = targetBounds.centerY() - TARGET_RADIUS - TARGET_PADDING - totalTextHeight > 0

        val left = Math.min(textBounds.left, targetBounds.left - targetRadius)
        val right = Math.max(textBounds.right, targetBounds.right + targetRadius)

        val titleHeight: Int
        if(titleLayout == null) {
            titleHeight = 0
        } else {
            titleHeight = titleLayout!!.height
        }

        val centerY: Int
        if(onTop) {
            centerY = targetBounds.centerY() - TARGET_RADIUS - TARGET_PADDING - totalTextHeight + titleHeight
        } else {
            centerY = targetBounds.centerY() + TARGET_RADIUS + TARGET_PADDING + titleHeight
        }
        return intArrayOf((left + right) / 2, centerY)
    }

    fun getTotalTextHeight(): Int {
        if(titleLayout == null) {
            return 0
        }

        if(descriptionLayout == null) {
            return titleLayout!!.height + TEXT_SPACING
        }

        return titleLayout!!.height + descriptionLayout!!.height + TEXT_SPACING
    }

    fun getTotalTextWidth(): Int {
        if(titleLayout == null) {
            return 0
        }

        if(descriptionLayout == null) {
            return titleLayout!!.width
        }

        return Math.max(titleLayout!!.width, descriptionLayout!!.width)
    }

    fun inGutter(y: Int): Boolean {
        if(bottomBoundary > 0) {
            return y < GUTTER_DIM || y > bottomBoundary - GUTTER_DIM
        } else {
            return y < GUTTER_DIM || y > height - GUTTER_DIM
        }
    }

    fun maxDistanceToPoints(x1: Int, y1: Int, bounds: Rect): Int {
        val tl = distance(x1, y1, bounds.left, bounds.top)
        val tr = distance(x1, y1, bounds.right, bounds.top)
        val bl = distance(x1, y1, bounds.left, bounds.bottom)
        val br = distance(x1, y1, bounds.right, bounds.bottom)
        return Math.max(tl, Math.max(tr, Math.max(bl, br))).toInt()
    }

    fun distance(x1: Int, y1: Int, x2: Int, y2: Int): Double {
        return Math.sqrt(Math.pow((x2-x1).toDouble(), 2.0) + Math.pow((y2-y1).toDouble(), 2.0))
    }

    fun invalidateViewAndOutline(bounds: Rect) {
//        invalidate(bounds) - this was the old way of doing this, but was deprecated. This could cause issues for the tutorial and the outlines, but will need testing to be sure.
        invalidate()
        if(outlineProvider != null && Build.VERSION.SDK_INT >= 21) {
            invalidateOutline()
        }
    }
}