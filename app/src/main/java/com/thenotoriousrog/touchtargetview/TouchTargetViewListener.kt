package com.thenotoriousrog.touchtargetview

import android.text.method.Touch

class TouchTargetViewListener {

    /**
     * Signals that the user has clicked inside of the target
     */
    fun onTargetClick(view: TouchTargetView) {
        view.dismiss(true)
    }

    /**
     * Signals that the user has long clicked inside of the target
     */
    fun onTargetLongClick(view: TouchTargetView) {
        onTargetClick(view)
    }

    /**
     * If cancelable, signals that the user has clicked outside of the outer circle
     */
    fun onTargetCancel(view: TouchTargetView) {
        view.dismiss(false)
    }

    /**
     * Signals that the user clicked on the outer circle
     */
    fun onOuterCircleClick(view: TouchTargetView) {} // do nothing internally

    /**
     * Signals that the tap target has been dismissed
     * @param userInitiated Whether the user caused this action or not.
     */
    fun onTargetDismissed(view: TouchTargetView, userInitiated: Boolean) {} // do nothing internally

//    companion object {
//
//
//    }

}