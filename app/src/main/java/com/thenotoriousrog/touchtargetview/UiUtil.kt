package com.thenotoriousrog.touchtargetview

import android.content.Context
import android.util.TypedValue

class UiUtil {

    companion object {

        /**
         * Returns a given pixel value in dp
         */
        fun dp(context: Context, value: Float): Int {
            return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, value, context.resources.displayMetrics).toInt()
        }

        /**
         * Returns pixel value in sp
         */
        fun sp(context: Context, value: Float): Int {
            return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, value, context.resources.displayMetrics).toInt()
        }

        /**
         * Returns the value of the desired theme integer attribute, or -1 if not found
         */
        fun themeIntAttr(context: Context, attr: String): Int {
            val theme = context.theme
            val value = TypedValue()
            val id = context.resources.getIdentifier(attr, "attr", context.packageName)
            if(id == 0) {
                return -1
            }

            theme.resolveAttribute(id, value, true)
            return value.data
        }

        /**
         * Modifies the alpha value of the given ARGB color
         */
        fun setAlpha(argb: Int, alpha: Float): Int {
            var newAlpha = alpha
            if(alpha > 1.0f) {
                newAlpha = 1.0f
            } else if(alpha <= 0.0f) {
                newAlpha = 0.0f
            }

            return (argb.ushr(24) * alpha).toInt() shl 24 or (argb and 0x00FFFFFF)
        }
    }

}