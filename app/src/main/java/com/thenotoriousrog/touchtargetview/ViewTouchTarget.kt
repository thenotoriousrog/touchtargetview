package com.thenotoriousrog.touchtargetview

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Rect
import android.graphics.drawable.BitmapDrawable
import android.support.v7.widget.ViewUtils
import android.view.View

/**
 * Base touch target that can be used to create TouchTargets for any view.
 */
open class ViewTouchTarget(private val view: View, title: CharSequence, description: CharSequence?) : TouchTarget(title, description) {

    override fun onReady(runnable: Runnable) {

        ViewUtil.onLaidOut(view, Runnable {

            val location = IntArray(2)
            view.getLocationOnScreen(location)
            bounds = Rect(location[0], location[1], location[0] + view.width, location[1] + view.height)

            if(view.width > 0 && view.height > 0 ) {
                val viewBitmap = Bitmap.createBitmap(view.width, view.height, Bitmap.Config.ARGB_8888)
                val canvas = Canvas(viewBitmap)
                view.draw(canvas)
                icon = BitmapDrawable(view.context.resources, viewBitmap)
                icon.setBounds(0,0, icon.intrinsicWidth, icon.intrinsicHeight)
            }

            runnable.run()
        })

        super.onReady(runnable)

    }

}