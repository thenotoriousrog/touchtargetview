package com.thenotoriousrog.touchtargetview

import android.os.Build
import android.support.v4.view.ViewCompat
import android.view.View
import android.view.ViewManager
import android.view.ViewTreeObserver
import java.lang.Exception

class ViewUtil {

    companion object {

        /**
         * Returns whether or not the view has been laid out
         */
        fun isLaidOut(view: View): Boolean {
            return ViewCompat.isLaidOut(view) && view.width > 0 && view.height > 0
        }

        /**
         * Executes the given runnable when the view is laid out
         */
        fun onLaidOut(view: View, runnable: Runnable) {
            if(isLaidOut(view)) {
                runnable.run()
                return
            }

            val observer = view.viewTreeObserver

            observer.addOnGlobalLayoutListener(object: ViewTreeObserver.OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    val trueObserver: ViewTreeObserver
                    if(observer.isAlive) {
                        trueObserver = observer
                    } else {
                        trueObserver = view.viewTreeObserver
                    }
                    removeOnGlobalLayoutListener(trueObserver, this)
                    runnable.run()
                }
            })
        }

        fun removeOnGlobalLayoutListener(observer: ViewTreeObserver, listener: ViewTreeObserver.OnGlobalLayoutListener) {
            if(Build.VERSION.SDK_INT >= 16) {
                observer.removeOnGlobalLayoutListener(listener)
            } else { // Note: removeGlobalOnLayoutListener is deprecated for sdk versions above 16. Only if it is below 16 sdk version can this work.
                observer.removeGlobalOnLayoutListener(listener)
            }
        }

        fun removeView(parent: ViewManager, child: View) {
            try {
                parent.removeView(child)
            } catch (ignored: Exception) {
                // Sometimes there are modified versions of Android. i.e. ROMs. Poor implementation of ROMs can lead to a buggy ViewGroup implementation
                // This exception will stop that from causing the app that uses TouchTargetView from dying as well.
            }
        }
    }

}